<style>
.text{
	width:200px;
	height:30px;
}
</style>

<html>

<head>

	<title>
		Add Teacher
	</title>
</head>

<body>

	<form action="<?= URL::to('/save')?>" method="post">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<a href="<?= URL::to('/')?>">Back</a><br>
	<table style="width:80%;margin:auto;overflow:auto;" text-align:center;>
		<center>
		<p style="color:red">{{$errors->first('teacher_name')}}</p>
		<p style="color:red">{{$errors->first('phone')}}</p>
	</center>

		<tr>
			<td>Teacher Name</td>
			<td>:</td>
			<td><input type="text" name="teacher_name" class="text">
		</tr>

		<tr>
			<td>Gender</td>
			<td>:</td>
			<td><select name="gender" class="text">
				<option value="0">Male</option>
				<option value="1">Female</option>
			</select>
		</tr>

		<tr>
			<td>Phone No</td>
			<td>:</td>
			<td><input type="text" name="phone" class="text">
		</tr>

		<tr>
			<td></td>
			<td></td>
			<td><input type="submit" value="Save" class="text">
		</tr>

	</table>


	
	</body>

</html>