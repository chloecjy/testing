@extends('app')
<head>
<script type="text/javascript" src="/ckeditor/ckeditor/ckeditor.js"></script>
</head>

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<ul class="nav nav-tabs">
  
  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" href="{{ url('/post/') }}" role="button" aria-expanded="false" >
      New Post 
    </a>
   
  <li role="presentation" class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
      Posts <span class="caret"></span>
    </a>
   <ul class="dropdown-menu" role="menu">
      1
    </ul>

  </li>

</ul>


				<div class="panel-body">
					<h2>Creating New Post</h2>
					<hr>
					<br>

					

						<form class="form-horizontal" role="form" method="POST" action="{{ url('/AuthorsController/create') }}">
						    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  							<div class="form-group">
    							<label for="inputEmail3" class="col-sm-2 control-label"><b>Title</b></label>
   								 <div class="col-sm-10">
      								<input type="text" class="form-control" id="inputEmail3" placeholder="Title" name="title" value="{{ old('title') }}">
    							</div>
  							</div>

  							<div class="form-group">
    							<label for="inputPassword3" class="col-sm-2 control-label"><b>Content</b></label>
    								<div class="col-sm-10">
     									 <textarea class="ckeditor" id="ckeditor" placeholder="Content" cols="50" rows="30" name="content" value="{{ old('content') }}"></textarea>
   									 </div>
  							</div>


  							
  							<div class="form-group">
    							<div class="col-sm-offset-2 col-sm-10">
      								<button type="submit" class="btn btn-default">Submit</button>
    							</div>
  							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
