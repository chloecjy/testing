@extends('app')
<head>
<script type="text/javascript" src="/ckeditor/ckeditor/ckeditor.js"></script>
</head>

@section('content')
<h1> Add New Author</h1>


{!!Form::open(array('url' => '/authors/create','POST'))!!}

<p>
{!!Form::label('title','Title:')!!}<br/>
{!!Form::text('name')!!}
</p>

<p>
{!!Form::label('content','Content:')!!}<br/>
{!!Form::textarea('content')!!}
</p>

<p>
{!!Form::submit('Add Author')!!}

</p>

{!!Form::Close()!!}

@endsection
