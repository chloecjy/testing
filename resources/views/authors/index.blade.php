@extends('default')

@section('content')
<h1>Authors Home Page </h1>
<a href="/authors/new" > New Post</a>

<ul>
	@foreach($authors as $author)
	<li>{{$author->title}}</li>
	@endforeach
</ul>
@endsection
