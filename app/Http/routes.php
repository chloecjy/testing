<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

Route::get('/', 'Teacher@index');
Route::get('/form', 'Teacher@form');

Route::post('/save',array('uses'=>'Teacher@save'));

Route::get('home', 'HomeController@index');

Route::get('authors', 'authors@index');


Route::get('/authors/new',function()
{
	return view('/authors/new');
});


Route::post('authors/create',array('uses'=>'authors@create'));

Route::get('/post',function()
{
	return view('post');
});



Route::get('/new',function()
{
	return view('/authors/new');
});




Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	//'post' => 'Post\PostController',
]);


