<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class Teacher extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View('Teacher/index');
	}


	public function form()
	{ 
		return View('Teacher/form');
	}

	public function save()
	{ 
		$validation=array(
			'teacher_name'=>'required',
			'phone'=>'required',
			);

		$vl=validator::make(Input::all(),$validation);
		if ($vl->fails())
		{
			return Redirect::to ('form')->withErrors($vl);

		}
		else
		{
			$postteacher=Input::all();
			$data=array('teacher_name'=>$postteacher['teacher_name'],
						'gender'=>$postteacher['gender'],
						'phone'=>$postteacher['phone']);
			$check=0;
			$check=DB::table('teacher')->insert($data);
			if($check>0)
			{
				return Redirect('/');
			}
			else
			{
				return Redirect::to ('/form');
			}
		}
	}


}
