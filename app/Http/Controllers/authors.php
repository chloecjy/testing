<?php namespace App\Http\Controllers;


use Input;
use author;



class Authors extends Controller{
	
	public $restful=true;

	public function index(){
		return view('/authors/index')
		->with ('title','Authors and Books')
		->with ('authors',Authors :: get() );

	}

	

	public function create(){

		Authors::create(array(

			'title'=>Input::get('title'),
			'content'=>Input::get('content')


		));

	return Redirect::to_route('authors')
			->with('message','The author was created successfully!');


	}
}