<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('password_resets', function(Blueprint $table)
		{
		$table->increments('id');
		$table->string('title');
		$table->string('content')->unique();
		$table->integer('published_on')->unsigned();
		$table->timestamps();
	});

		
			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema ::drop('posts');
	}

}
